## What are AtumScan Templates?
AtumScan templates are the cornerstone of the AtumScan scanning engine. AtumScan templates enable precise and rapid scanning across various protocols like TCP, DNS, HTTP, and more. They are designed to send targeted requests based on specific vulnerability checks, ensuring low-to-zero false positives and efficient scanning over large networks.

## YAML
AtumScan templates are based on the concepts of `YAML` based template files that define how the requests will be sent and processed. This allows easy extensibility capabilities to AtumScan. The templates are written in `YAML` which specifies a simple human-readable format to quickly define the execution process.

## Universal Language for Vulnerabilities
AtumScan Templates offer a streamlined way to identify and communicate vulnerabilities, combining essential details like severity ratings and detection methods. This open-source, community-developed tool accelerates threat response and is widely recognized in the cybersecurity world.